FROM gocd/gocd-agent-ubuntu-16.04:v20.10.0
USER root
WORKDIR /app
RUN curl -O https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.5.0.2216-linux.zip \
    && unzip -q sonar-scanner-cli-4.5.0.2216-linux.zip \
    && mkdir /sonar-scanner \
    && mv sonar-scanner-4.5.0.2216-linux/* /sonar-scanner \
    && rm sonar-scanner-cli-4.5.0.2216-linux.zip -r sonar-scanner-4.5.0.2216-linux \
    && curl -sL https://deb.nodesource.com/setup_14.x | bash - \
    && apt-get install -y nodejs
